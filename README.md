### Site Vitrine
Fait avec Vue 3

Liste des informations a afficher sur le site : 
* Lister les fonctionnalites du projet.
* Demo (video et screenshot)
* Use-cases
* Lien vers le site du client Web
* Lien vers les apps store pour les clients mobiles
* Lien vers le gitlab
* Lien vers les reseaux sociaux si y'en a
* FAQ
* Qui sommes nous
* Contact
