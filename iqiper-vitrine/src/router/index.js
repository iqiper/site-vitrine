import { createWebHistory, createRouter } from "vue-router";
import Home from '@/components/Home.vue';
import NotFound from '@/components/NotFound.vue';
import Documentation from '@/components/Documentation.vue';
import FAQ from '@/components/FAQ.vue';
import Us from '@/components/Us.vue';

const routes = [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: {
              title: 'Home'
            }
        },
        {
            path: '/documentation',
            name: 'Doc',
            component: Documentation,
            meta: {
              title: 'Documentation'
            }
        },
        {
            path: '/FAQ',
            name: 'FAQ',
            component: FAQ,
            meta: {
              title: 'FAQ'
            }
        },
        {
            path: '/us',
            name: 'us',
            component: Us,
            meta: {
              title: 'Us'
            }
        },
        {
            path: '/404',
            name: '404',
            component: NotFound,
            meta: {
              title: '404'
            }
        },
    ]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
