import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
      next('/404');
    } else {
      next();
    }
  });
  

createApp(App).use(router).mount('#app')
