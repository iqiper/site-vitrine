module.exports = {
    extends: [
      'plugin:vue/vue3-recommended',
    ],
    rules: {
      'vue/no-unused-vars': 'error',
      'max-len': 0,
    }
  }