# Credits to : https://fr.vuejs.org/v2/cookbook/dockerize-vuejs-app.html
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY iqiper-vitrine/package*.json ./
RUN npm install
COPY iqiper-vitrine .
RUN npm run build

# étape de production
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
